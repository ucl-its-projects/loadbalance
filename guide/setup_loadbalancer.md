author:            19A ITS1
summary:           Set up a HA proxy system
id:                setup_loadbalancer
categories:        HA proxy example system
environments:      Virtual machines
status:            wip
feedback link:     MON@UCL.DK
analytics account: Always 0

# Setup HA proxy example

## Introduction

This guide will help set up a multi VM system using a `HA proxy` load balancer to load balance between to web servers.

The resulting topology is a follows

![Topology](diagrams/diagram.png)

The guide assumes that the system is set up in VMWare workstation, where `vmnet1` is a host-only network and `vmnet8` is a NAT'ed network.

See the [VMWare workstation manual](https://docs.vmware.com/en/VMware-Workstation-Pro/15.0/workstation-pro-15-user-guide.pdf) (section 8.1, p. 211) for details.

The network addresses of the vnnets differ from installation to installation, so we only decided on the last octets.

Prerequisites are a recent [Debian](https://debian.org) minimal virtual machine. This is assumed to be called `deb10-base`.

In the guide, `vmnet1` has the network address `192.168.36.0/24`.

## Set up web servers

1. In VMware workstation, clone `deb10-base` and name the new machine `deb-web-a`
2. Ensure that it is connected to `vmnet8`
3. On `deb-web-a`, log in as root using console
4. Install nginx: `apt-get install nginx`
5. Edit the default index file located at `/var/www/html/index.nginx-debian.html` and change `<h1>Welcome ... </h1>` to `<h1>Welcome ... on web A</h1>`
6. Change the IP address by editing `/etc/network/interfaces`. A sample file is [here](network-interfaces-web-a.txt)
7. In vmware workstation, for the VM select `settings -> hardware -> network adapter` and set the network adapter to `custom` and the value `/dev/vmnet1`
8. Test the configuration of the webserver by using a web browser on the host and connect to `http://192.168.36.50`
9. Install `deb-web-b` by redoing 1-8
here
   Replace `web A` with `web B`

   Replace `192.168.36.50` with `192.168.36.51`, whereever it is applicable.

You should now have 2 VMs with webservers with no internet access.

## Setup HA proxy

1. In VMware workstation, clone `deb10-base` and name the new machine `deb-load-balancer`
2. Change the network config of the VM, so it has 2 NIC; one connected to `vmnet1` and the other connected to `vmnet8`
3. Boot the VM
4. On the VM, log in as root
5. Use `ip address`, and compare the IPs with the exected IPs to ensure you know which interface is connected to which network.
6. Edit `/etc/network/interfaces` following the template [here](network-interfaces-load-balancer.txt)

   Notice the use of `.2` for gateway and DNS (as opposed to the more default `.1`)

7. Install HAproxy: `apt-get install haproxy`
8. Change config to include a `frontend` and a `backend` entry. See [here](haproxy.cfg.txt) for template.
9. Reboot `deb-load-balancer`


Instructions for ubuntu are [here](https://tecadmin.net/how-to-setup-haproxy-load-balancing-on-ubuntu-linuxmint/)

## Test

Test by connecting to `http://192.168.203.30` and press reload.

You should see from the heading the you shift between `deb-web-a` and `deb-web-b`.

## Closing remarks

* No firewall rules
* You should use ssh keys to access all servers.
* For at better scenarios, the webservers must be able to update either from the internet or some other internal VLAN.
* You should wireshark on both sides the loadbalancer to see how it changed the HTTP headers.
